import re, urlparse
import json
import sys

print "Usage: python asciiUrlhIssue.py inputJsonFileName outputJsonFileName"

def urlEncodeNonAscii(b):
    return re.sub('[\x80-\xFF]', lambda c: '%%%02x' % ord(c.group(0)), b)

def iriToUri(iri):
    parts= urlparse.urlparse(iri)
    return urlparse.urlunparse(
        part.encode('idna') if parti==1 else urlEncodeNonAscii(part.encode('utf-8'))
        for parti, part in enumerate(parts)
    )

file=sys.argv[1]
file_out=sys.argv[2]
fout=open(file_out,"w")
# print file
with open(file,"r") as fin:
	for line in fin:
		data=json.loads(line)
		thumnail=data.get('thumbnail','')
		if thumnail!='':
			data['thumbnail']=iriToUri(data['thumbnail'])
		images=[]
		# print data['images']
		for img in data['images']:
			# continue
			images.append(iriToUri(unicode(img)))
		data['images']=images
		json.dump(data,fout)
		fout.write("\n")
		# iriToUri(u"")