"""Summary
"""
import requests
import cv2
import numpy as np
import os
import sys

def mapperSolr(title_String):
                                """Get thumbnail hash from solr
                                
                                Args:
                                    title_String (TYPE): Description
                                """
                                solr="http://172.17.1.162:8983/solr/cbir_mapper_bbb/select"
                                # queryString = "( "+ " ".join(urlh_hash_list)+ " )"
                                queryString ="( "+title_String+" )"
                                start = 0
                                rows = 1
                                outputThumbnailHash={}
                                outputUrlh=[]
                                outputList=[]
                                while(1):
                                                param = {}
                                                param["q"] = "title:%s"%(queryString)
                                                param["fl"] = "thumbnail_hash,title,urlh"
                                                param["wt"] = "json"
                                                param["shards"] = "172.17.1.149:8983/solr/cbir_mapper_bbb,172.17.1.162:8983/solr/cbir_mapper_bbb,172.17.1.149:8984/solr/cbir_mapper_bbb,172.17.1.162:8984/solr/cbir_mapper_bbb"
                                                param["start"] = start
                                                param["rows"] = 100
                                                dataFetched = requests.get(solr,params=param)
                                                dataFetchedJson =dataFetched.json()
                                                print len(dataFetchedJson)

                                                try:   
                                                        if len(dataFetchedJson["response"]["docs"])==0:
                                                                        break
                                                except Exception as e:
                                                        print e
                                                        return outputUrlh
                                                        break
                                                outputUrlh+=[{'title_String':title_String,'urlh':data['urlh'],'thumbnail_hash':data['thumbnail_hash'],'title':data['title']}for data in dataFetchedJson["response"]["docs"]]
                                                if len(outputUrlh)>100:
                                                        break
                                                start = start+rows
                                return outputUrlh

def fetch_image_warc(thumbnail_required,OutputDir="out"):
    """Fetch and downlod image from warc
    
    Args:
        thumbnail_required (TYPE): Description
        OutputDir (str, optional): Description
    """
    image_fetch_api_address = "http://fetch.images.dwservices.com/fetchCand?"
    fetchDataParam = {}
    fetchDataParam['thumbnailList'] = ",".join(thumbnail_required)
    try:
        data = requests.get(url=image_fetch_api_address,params=fetchDataParam)
        jsonData = data.json()
    except Exception as e:
        print e
        return  {}
    thumbnail_map = {}
    byteList = jsonData["byte_array_list"]
    for itr in byteList:
        byteFetched = itr["byte"].decode("base64")
        thumbnail_map[itr['id']] = byteFetched
        arr = np.asarray(bytearray(byteFetched), dtype=np.uint8)
        imageMatFile = cv2.imdecode(arr, -1)
	print itr['id']
        # cv2.imwrite( OutputDir+"/"+itr['id'],imageMatFile);
        cv2.imwrite( OutputDir+"/"+itr['id']+".jpg",imageMatFile)
    return

if __name__=="__main__":
        """System arguments:- search_text, outputDirectory
        """
        searchString="*"+sys.argv[1]+"*"
        outputDir=sys.argv[2]
        mapper_output= mapperSolr(searchString)
        thumbnail_hash_cache=[]
        with open("data_"+sys.argv[2].split("/")[-1],"w") as fout:
                for itr in mapper_output:
                        if itr['thumbnail_hash'] not in thumbnail_hash_cache:
                                fetch_image_warc([itr['thumbnail_hash']],OutputDir=outputDir)
                                thumbnail_hash_cache.append(itr['thumbnail_hash'])
