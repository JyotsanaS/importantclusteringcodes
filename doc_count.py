import json
import sys

with open("tool_after_clustering_cbir_Nordstrom_New_sku_0724.json","r") as fin, open("image_clustering_results.json","w") as fout:
	doc_count=0
	for line in fin:
		data=json.loads(line)
		doc_count+=len(data['docs'])-1
		data['comp_list']=data['original_comp_list']
		data['tagged_data']['competitors']=data['comp_list']
		json.dump(fout,data)
		fout.write("\n")
	print doc_count
