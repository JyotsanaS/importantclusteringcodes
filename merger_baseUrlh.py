import json
import sys
import requests

text={}
text_afterCluster=sys.argv[1]
img_afterCluster=sys.argv[2]
mergedFile=sys.argv[3]

def getDataFromSolr(solr,url_list):
    params = {
        "country": "usa",
        "q": "urlh:(" + " ".join(url_list) + ")",
        "fl": "urlh,base_product_urlh,color,source",
        "rows": 1000,
    }

    response = requests.get(solr, params=params).json()
    response = response["response"]
    docs=[]
    if "docs" in response:
		docs = response["docs"]
    return docs

with open(text_afterCluster,"r") as fin:
	for line in fin:
		data=json.loads(line)
		seed=data['seed']
		docs=data['docs']
		docs.remove(seed)
		text[seed]={}
		text[seed]['docs']=docs
		text[seed]['comp_list']=data['comp_list']

fout=open(mergedFile,"w")

count_text=0
count_actual=0
count_image=0
count_final=0
count_bundles=0
count_matches_naive=0
count_matches_smart=0
with open(img_afterCluster,"r") as fin:
	for line in fin:
		data=json.loads(line)
		seed=data['seed']
		docs=data['docs']
		try:
			docs.extend(text[seed]['docs'])
		except:
			print "Text not present"
		docs=list(set(docs))
		text_solr_base=getDataFromSolr("http://api.solr.dweave.net/usa",docs)
		print data['docs']
		print text[seed]['docs']
		print text_solr_base
		base_product_urlh={}
		final_doc=[]
		for itr in text_solr_base:
			try: 
				if itr['base_product_urlh'] not in base_product_urlh:
					base_product_urlh[itr['base_product_urlh']]=[]
				if itr['color'] not in base_product_urlh[itr['base_product_urlh']]:
					base_product_urlh[itr['base_product_urlh']].append(itr['color'])
					final_doc.append(itr['urlh'])
			except:
				final_doc.append(itr['urlh'])
		final_doc=list(set(final_doc))
		print "Final_doc: ",final_doc
		count_image+=len(data['comp_list'])
		data['comp_list'].extend(text[seed]['comp_list'])
		data['comp_list']=list(set(data['comp_list']))
		data["tagged_data"]["competitors"]=data['comp_list']
		data['docs']=final_doc
		json.dump(data,fout)
		'''Stats- How many matches are proposed'''
		count_matches_naive+=len(docs)
		count_matches_smart+=len(final_doc)
		count_text+=len(text[seed]['comp_list'])
		count_actual+=len(data['tagged_comp_list'])
		count_final+=len(data['comp_list'])
		if len(docs)>1:
			count_bundles+=1
		fout.write("\n")
print "count_Actual: ",count_actual
print "count_text: ",count_text
print "count_image: ",count_image
print "count after merging: ",count_final
print "count bundles after merging: ",count_bundles
print "Original Matches:",count_matches_naive
print "Updated Matches:",count_matches_smart
