import json
import sys

def remFilter(input,output):
	with open(input,"r") as fin,open(output,"w") as fout:
		for line in fin:
			data=json.loads(line)
			matches=data['matches']
			count=0
			newMatch={}
			for key in matches:
				for itr in matches[key]:
					if itr['match']=='doc':
						if key not in newMatch:
							newMatch[key]=[]
						newMatch[key].append(itr)
			if len(newMatch)>0:
				data['matches']=newMatch
				json.dump(data,fout)
				fout.write("\n")

if __name__ == '__main__':
	if len(sys.argv)!=3:
		print "Argument needed: "
		print "Arg 1: Path for matching combined file"
		print "Arg 2: Path for output file"
	print "This script removes suggestion matches from matching combined file."
	print "Also we can add additional filter on matching combined file"	

	inputfile=sys.argv[1]
	outputfile=sys.argv[2]
	remFilter(inputfile,outputfile)
