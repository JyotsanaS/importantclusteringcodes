import sys
sys.path.insert(0, 'src/')
import json
import requests
import time
import gzip
from urlparse import urljoin
import hashlib
from datetime import datetime
from fluent import sender
from fluent import event
import socket
import unicodedata
from main import CBIRPipeline
import sys

try:
    from kafkalib import kafka_consumer
except Exception, e:
    print "Unable to import kafka_consumer"


def get_ip_address():
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        return s.getsockname()[0]

#def logData(data):
        ## will push data to elstic search
    #    print data
 #       sender.setup("cbir_indexing",host="localhost",port=24224)
 #       event.Event('status',data)



def is_ascii(text):
		return all(ord(c) <128 for c in text)

def unicode_to_string(text):
		if is_ascii(text):
			return text
		else:
			try:
				text = unicode(text,'utf-8')
			except:
				text = text	
			text = unicodedata.normalize('NFKD', text).encode('ascii','ignore')		
			return text


def writeJsonFile(listOfDataToPush,seed_file_path):
	with open(seed_file_path,"w") as fileWriter:
		for itr in listOfDataToPush:
			json.dump(itr,fileWriter)
			fileWriter.write("\n")




def main():

	# config file path is required as argument
	config_path = sys.argv[1]
	#input topic for seeds
	topicForMapperPush = "sm_input_cbir_clustering"
	consumerForMapperPush = "sm_input_cbir_clustering_consumer_1"
	kafkaConumerForMapper =  kafka_consumer.kafka(topic = topicForMapperPush,consumer_group=consumerForMapperPush,read_from_beginning=False,runForever=False)
	ipAdd = get_ip_address()

	consumer = kafkaConumerForMapper.get_consumer()

	listOfDataToPush = []
	tracking_code_map = []

	error  = 0
	count = 0
	path_to_store_seed = "/home/semantics/cfit/cbir_cnn/results/seeds"
	path_to_store_clustering_output = "/home/semantics/cfit/cbir_cnn/results/clustering_output"

	while(True):
		## for msg in consumer do the processing 
		for msg in consumer: 
			value = msg.value
			# wprint value
			timeout = time.time() + 10
	#		print "i am in"
			count+=1
			print count,timeout
			#continue
			try:
				data = json.loads(value)
				## extending the information of Bhash 
				# urlh = data["purlh"]
				# imageList = data["thumbnails"]
				# source = data.get("source","")
				# url = data["purl"]
				# title = data.get("title","")

				listOfDataToPush.append(data)

				# tracking_code = data.get("tracking_code","cbir_mapper_solr_push")
				# try:
				# 	for imageEnt in imageList:
				# 		# urlList.append(ent)
				# 		hashImageObj = hashlib.sha1(imageEnt)
				# 		imageHashExt = hashImageObj.hexdigest()
				# 		if imageHashExt== "da39a3ee5e6b4b0d3255bfef95601890afd80709":
				# 			print "invalid thumbnail",data
				# 			continue

						
				# 		dataFormater = {}
						
				# 		## extending the information of Bhash 
						
				# 		dataFormater["url"] = url
				# 		dataFormater["thumbnail"] = imageEnt
				# 		dataFormater["source"] = source
				# 		dataFormater["id"] = imageHashExt+"_"+urlh
				# 		dataFormater["thumbnail_hash"] = imageHashExt
				# 		dataFormater["path"] = ""
				# 		dataFormater["urlh"] = urlh
				# 		dataFormater["title"] = ""
				# 		try:
				# 			dataFormater["title"] = unicode_to_string(title)
				# 		except Exception as e:
				# 			print "Warning: title is not in correct format",title
				# 		tracking_code_map.append((tracking_code,dataFormater))
				# 		#print "urlh: ",urlh,"thumbnailHash: ",imageHashExt

				# 		listOfDataToPush.append(dataFormater)
				# except Exception as e:
				# 	print "urlh error ",e
				# 	print data
				# 	error+=1
				# 	logData({"cbir_index_id":tracking_code,\
				# 	"cbir_index_status":501,"cbir_index_msg":str(e),"cbir_index_time_created":str(datetime.now()),\
				# 	"cbir_index_process":"kafkaCBIRMapperPush","cbir_index_time":0.0,"cbir_ip":ipAdd,"cbir_index_urlh":urlh})

				# Run either when batch is of 1000 size or when for sometime no new requests are coming(handling last batch < bachsize)
				if len(listOfDataToPush) >=100 :
					#for not overwitting the seed file because names are generated from time
					time.sleep(1)
					timeStart = time.time()
					# statusUpload, statusCommit = indexDocs(listOfDataToPush,solrAddToPush=solrAddToPush)
					# time_taken = time.time() - solrPushTimeStart
					# avg_time_taken = time_taken/len(listOfDataToPush) #time taken per urlh

					time_current = time.strftime("%Y%m%d-%H%M%S")
					seed_file_name = "seed_" + time_current
					clustering_file_name = "clustering_" + time_current  
					seed_file_path = path_to_store_seed+"/"+seed_file_name
					writeJsonFile(listOfDataToPush,seed_file_path)

					clustering_file_path = path_to_store_clustering_output +"/"+clustering_file_name
					cbir_obj = CBIRPipeline(config_path,seed_file_path,clustering_file_path)
					cbir_obj.run_cbir()
					time_taken = time.time() - timeStart

					print "time taken for run clustering: ",time_taken, "and results are saved to :", clustering_file_path

					# #this is for logging in elastic search
					# for tracking_id,data_itr in tracking_code_map:
					# 	if (statusUpload.status_code==200) and (statusCommit.status_code==200):
					# 		msg = "success"
					# 		index_status = 201
					# 	else:
					# 		msg = str(statusUpload.content)
					# 		index_status = 501


					# 	logData({"cbir_index_id":tracking_id,"cbir_index_source":data_itr['source'],"cbir_index_image":data_itr['thumbnail_hash'],\
					# 		"cbir_index_urlh":data_itr['urlh'],"cbir_index_status":index_status,"cbir_index_msg":msg,"cbir_index_time_created":str(datetime.now()),\
					# 		"cbir_index_process":"kafkaCBIRMapperPush","cbir_index_time":avg_time_taken,"cbir_upload_status":statusUpload.status_code,"cbir_commit_status":statusCommit.status_code,\
					# 		"cbir_ip":ipAdd})

					listOfDataToPush=[]
					tracking_code_map=[]
					break
				else:
					continue

			except Exception as e:
				print "error",e 
	                        #logData({"cbir_index_id":"cbir_mapper_solr_push",\
	                         #        "cbir_index_status":501,"cbir_index_msg":str(e),"cbir_index_time_created":str(datetime.now()),\
	                          #       "cbir_index_process":"kafkaCBIRMapperPush","cbir_index_time":0.0,"cbir_ip":ipAdd}		
		else:
			#only executing if we have some leftovers
			if len(listOfDataToPush) >1 :

				timeStart = time.time()

				time_current = time.strftime("%Y%m%d-%H%M%S")
				seed_file_name = "seed_" + time_current
				clustering_file_name = "clustering_" + time_current  
				seed_file_path = path_to_store_seed+"/"+seed_file_name
				writeJsonFile(listOfDataToPush,seed_file_path)

				clustering_file_path = path_to_store_clustering_output +"/"+clustering_file_name
				cbir_obj = CBIRPipeline(config_path,seed_file_path,clustering_file_path)
				cbir_obj.run_cbir()
				time_taken = time.time() - timeStart

				print "time taken for run clustering: ",time_taken, "and results are saved to :", clustering_file_path

				# #this is for logging in elastic search
				# for tracking_id,data_itr in tracking_code_map:
				# 	if (statusUpload.status_code==200) and (statusCommit.status_code==200):
				# 		msg = "success"
				# 		index_status = 201
				# 	else:
				# 		msg = str(statusUpload.content)
				# 		index_status = 501


				# 	logData({"cbir_index_id":tracking_id,"cbir_index_source":data_itr['source'],"cbir_index_image":data_itr['thumbnail_hash'],\
				# 		"cbir_index_urlh":data_itr['urlh'],"cbir_index_status":index_status,"cbir_index_msg":msg,"cbir_index_time_created":str(datetime.now()),\
				# 		"cbir_index_process":"kafkaCBIRMapperPush","cbir_index_time":avg_time_taken,"cbir_upload_status":statusUpload.status_code,"cbir_commit_status":statusCommit.status_code,\
				# 		"cbir_ip":ipAdd})

				listOfDataToPush=[]
				tracking_code_map=[]

if __name__ == "__main__":
	main()	
			
	
