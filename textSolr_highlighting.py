"""Summary:
Code for finding highlighted words in "q"
"""
import requests
import cv2
import numpy as np
import os
import sys
import json

def getDataFromTextSolr(solr,keyword):
    params = {
        "country": "usa",
        "q": "title: "+keyword,
        # "fl": "urlh,base_product_urlh,color,source",
        "hl":"on",
        "hl.fl":"title",
        "rows": 10000,
    }

    response = requests.get(solr, params=params).json()
    response=response["highlighting"]
    # print response
    NewKeywords=[]
    for eachUrlh in response:
        title=response[eachUrlh]['title']
        for eachTitle in title:
            first=eachTitle.find("<em>")
            last=eachTitle.rfind("</em>")+5
            key=eachTitle[first:last]
            key=key.replace("<em>","")
            key=key.replace("</em>","")
            if key not in NewKeywords:
                NewKeywords.append(key)
    return NewKeywords, len(NewKeywords)


if __name__=="__main__":
        """System arguments:- search_text, outputDirectory, numOfImg
        """
        # searchString="*"+sys.argv[1]+"*"
        searchString="(*scoop neck*)"
        print getDataFromTextSolr("http://api.solr.dweave.net/usa",searchString)