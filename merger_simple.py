import json

text={}
text_afterCluster='rei_seed_may_05_09_19_07_14_36_all_seeds'
img_afterCluster='rei_us_may_flag1_0508_tocluster.json_tool'

with open(text_afterCluster,"r") as fin:
	for line in fin:
		data=json.loads(line)
		seed=data['seed']
		docs=data['docs']
		docs.remove(seed)
		text[seed]=docs

fout=open("mergedclusteringtech_tool.json","w")

with open(img_afterCluster,"r") as fin:
	for line in fin:
		data=json.loads(line)
		seed=data['seed']
		docs=data['docs']
		docs.extend(text[seed])
		docs=list(set(docs))
		json.dump(data,fout)
		fout.write("\n")

